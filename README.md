## Image-based Biometry

This repository is part of the Task 3 for a FRI course in Image-based Biometry.
Data is not included.

Main findings are represented here:

1. Base model comparison with different datasets and final layers.

![](figures/cmc_model_comparison.png)

2. Xception based model comparison with different datasets and Data Augmentation

![](figures/cmc_Xception_model_comparison.png)

For more information create an issue.