import cv2
import os
import pandas as pd


def construct_dataset(dataset_name="no_preprocessing", preprocessing_function=None):

    annotations = pd.read_csv(
        "data/perfectly_detected_ears/annotations/recognition/ids.csv",
        header=None,
    )
    annotations.columns = ["fn_full", "label"]
    annotations["dataset"] = annotations.apply(
        lambda row: row["fn_full"].split("/")[0], axis=1
    )
    annotations["fn"] = annotations.apply(
        lambda row: row["fn_full"].split("/")[1], axis=1
    )

    DATA_DIR = "data/perfectly_detected_ears"

    if dataset_name not in os.listdir(DATA_DIR):
        os.system(f"mkdir {DATA_DIR}/{dataset_name}")
        os.system(f"mkdir {DATA_DIR}/{dataset_name}/train")
        os.system(f"mkdir {DATA_DIR}/{dataset_name}/test")

        for split in ["train", "test"]:
            for i in range(1, 101):
                os.system(f"mkdir {DATA_DIR}/{dataset_name}/{split}/{i}")

    for name, row in annotations.iterrows():

        img_path_in = f"{DATA_DIR}/{row['dataset']}/{row['fn']}"
        img_path_out = (
            f"{DATA_DIR}/{dataset_name}/{row['dataset']}/{row['label']}/{row['fn']}"
        )

        img = cv2.imread(img_path_in, cv2.IMREAD_UNCHANGED)
        img = cv2.resize(img, (150, 150), interpolation=cv2.INTER_AREA)

        if preprocessing_function is not None:
            img = preprocessing_function(img)

        cv2.imwrite(img_path_out, img)


def gaussian_blur(img):
    img = cv2.GaussianBlur(img, (3, 3), 0)
    return img


def laplacian(img):
    img_gauss = gaussian_blur(img)
    img_R = cv2.Laplacian(img_gauss, ddepth=cv2.CV_8U, ksize=3)
    return img_gauss + img_R


if __name__ == "__main__":

    construct_dataset()
    construct_dataset("gaussian", gaussian_blur)
    construct_dataset("laplacian", laplacian)
