import math
import numpy as np


class Evaluation:
    def compute_rank1(self, Y, y):
        classes = np.unique(sorted(y))
        count_all = 0
        count_correct = 0
        for cla1 in classes:
            idx1 = y == cla1
            if (list(idx1).count(True)) <= 1:
                continue
            # Compute only for cases where there is more than one sample:
            Y1 = Y[idx1 == True, :]
            Y1[Y1 == 0] = math.inf
            for y1 in Y1:
                s = np.argsort(y1)
                smin = s[0]
                imin = idx1[smin]
                count_all += 1
                if imin:
                    count_correct += 1
        return count_correct / count_all * 100

    def compute_rank1_onehot(self, y_true, y_pred):
        score = 0

        for true, pred in zip(y_true, y_pred):
            if true[np.argmax(pred)] == 1:
                score += 1

        return score / len(y_true)

    def compute_rank5_onehot(self, y_true, y_pred):
        score = 0

        for true, pred in zip(y_true, y_pred):
            if sum(true[np.argsort(pred)[::-1][:5]]) == 1:
                score += 1

        return score / len(y_true)

    def compute_rank10_onehot(self, y_true, y_pred):
        score = 0

        for true, pred in zip(y_true, y_pred):
            if sum(true[np.argsort(pred)[::-1][:10]]) == 1:
                score += 1

        return score / len(y_true)

    def cmc_curve(self, y_true, y_pred):
        score_dict = {i: 0 for i in range(1, 11)}

        for true, pred in zip(y_true, y_pred):

            sorted_args = np.argsort(pred)[::-1]

            for i in range(1, 11):

                if sum(true[sorted_args[:i]]) == 1:

                    for k in range(i, 11):
                        score_dict[k] += 1

                    break

        return [score_dict[i] / len(y_true) for i in range(1, 11)]


# Add your own metrics here, such as rank5, (all ranks), CMC plot, ROC, ...

# def compute_rank5(self, Y, y):
# 	# First loop over classes in order to select the closest for each class.
# 	classes = np.unique(sorted(y))

# 	sentinel = 0
# 	for cla1 in classes:
# 		idx1 = y==cla1
# 		if (list(idx1).count(True)) <= 1:
# 			continue
# 		Y1 = Y[idx1==True, :]

# 		for cla2 in classes:
# 			# Select the closest that is higher than zero:
# 			idx2 = y==cla2
# 			if (list(idx2).count(True)) <= 1:
# 				continue
# 			Y2 = Y1[:, idx1==True]
# 			Y2[Y2==0] = math.inf
# 			min_val = np.min(np.array(Y2))
# 			# ...
