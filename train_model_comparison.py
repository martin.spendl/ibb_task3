"""Script for training different base models"""
"""Implemented as shown : https://www.analyticsvidhya.com/blog/2020/08/top-4-pre-trained-models-for-image-classification-with-python-code/"""
import numpy as np
import pandas as pd
from metrics.evaluation_recognition import Evaluation
import tensorflow as tf
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras import layers
from tensorflow.keras import Model
from tensorflow.keras.callbacks import EarlyStopping

import tensorflow_ranking as tfr

mmr = tfr.keras.metrics.MRRMetric()


def train(
    train_dir="data/perfectly_detected_ears/train",
    test_dir="data/perfectly_detected_ears/test",
    epochs=10,
    base_model="efficient_net_B0",
    final_layers="small",
):

    train_data_generator = ImageDataGenerator(
        rescale=1.0 / 255.0,
        # rotation_range=0,
        # width_shift_range=0.2,
        # height_shift_range=0.2,
        # shear_range=0.2,
        # zoom_range=0.5,
        # horizontal_flip=False,
        validation_split=0.15,
    )

    test_data_generator = ImageDataGenerator(rescale=1.0 / 255.0)

    # dataset generators
    train_generator = train_data_generator.flow_from_directory(
        train_dir,
        batch_size=128,
        class_mode="categorical",
        target_size=(150, 150),
        subset="training",
        shuffle=True,
    )

    validation_generator = train_data_generator.flow_from_directory(
        train_dir,
        batch_size=128,
        class_mode="categorical",
        target_size=(150, 150),
        subset="validation",
    )

    test_generator = test_data_generator.flow_from_directory(
        test_dir,
        batch_size=128,
        class_mode="categorical",
        target_size=(150, 150),
        shuffle=False,
    )

    # selecting model
    if base_model == "efficient_net_B0":
        print("loading EfficientNetB0")
        eff_model = tf.keras.applications.EfficientNetB0(
            input_shape=(150, 150, 3),
            include_top=False,
            weights="imagenet",
            classes=100,
        )

    elif base_model == "efficient_net_B7":
        print("loading EfficientNetB7")
        eff_model = tf.keras.applications.EfficientNetB7(
            input_shape=(150, 150, 3),
            include_top=False,
            weights="imagenet",
            classes=100,
        )

    elif base_model == "ResNet50":
        print("loading ResNet50")
        eff_model = tf.keras.applications.ResNet50(
            input_shape=(150, 150, 3),
            include_top=False,
            weights="imagenet",
            classes=100,
        )

    elif base_model == "Xception":
        print("loading Xception")
        eff_model = tf.keras.applications.Xception(
            input_shape=(150, 150, 3),
            include_top=False,
            weights="imagenet",
            classes=100,
        )

    else:
        raise ValueError(f"Invalid model selected: {base_model}")

    for layer in eff_model.layers:
        layer.trainable = False

    inputs = layers.Flatten()(eff_model.output)

    if final_layers == "small":
        x = layers.Dense(10, activation="elu")(inputs)

    if final_layers == "wide_and_deep":
        wide = layers.Dense(128, activation="elu")(inputs)
        deep = inputs
        for i in range(5):
            deep = layers.Dense(16, activation="elu")(deep)
        x = layers.Concatenate()([wide, deep])
        # x = layers.Dropout(0.1)(x)

    if final_layers == "direct":
        x = inputs

    outputs = layers.Dense(100, activation="softmax")(x)

    model = Model(
        inputs=eff_model.input, outputs=outputs, name=f"{base_model}_for_ears"
    )

    print("Compiling the model")
    model.compile(
        optimizer="adam",
        loss="categorical_crossentropy",
        metrics=["accuracy", mmr],
    )

    print("Fitting the model")
    model.fit(
        train_generator,
        epochs=epochs,
        validation_data=validation_generator,
        callbacks=[
            EarlyStopping(
                monitor="val_mrr_metric",
                mode="max",
                patience=10,
                restore_best_weights=True,
            )
        ],
    )

    print("Evaluating model")
    y_pred = model.predict(test_generator)
    y_true = construct_one_hot_y_true(
        test_generator.labels, test_generator.class_indices
    )

    evaluator = Evaluation()
    evaluation = {
        "rank1": evaluator.compute_rank1_onehot(y_true, y_pred),
        "rank5": evaluator.compute_rank5_onehot(y_true, y_pred),
        "rank10": evaluator.compute_rank10_onehot(y_true, y_pred),
    }
    cmc_curve = evaluator.cmc_curve(y_true, y_pred)

    return (
        pd.Series(evaluation, name=f"{preprocessing}-{base_model}-{final_layers}"),
        cmc_curve,
    )


def construct_one_hot_y_true(labels, indices):
    y_true = []

    for cl in labels:
        zeros = np.zeros(len(indices.keys()))
        zeros[cl] = 1
        y_true += [zeros]

    return np.array(y_true)


if __name__ == "__main__":

    results = []
    cmc_curves = {}

    for preprocessing in ["no_preprocessing", "gaussian", "laplacian"]:
        for base_model in [
            "ResNet50",
            "Xception",
            "efficient_net_B0",
        ]:
            for final_layers in ["small", "wide_and_deep", "direct"]:

                output = train(
                    f"data/perfectly_detected_ears/{preprocessing}/train",
                    f"data/perfectly_detected_ears/{preprocessing}/test",
                    epochs=100,
                    base_model=base_model,
                    final_layers=final_layers,
                )

                results += [output[0]]
                cmc_curves[f"{preprocessing}-{base_model}-{final_layers}"] = output[1]

    results_df = pd.concat(results, axis=1, join="inner")
    results_df.index.name = "model"
    results_df.reset_index().to_csv(
        "data/multiple_model_comparison.tsv", sep="\t", index=False
    )

    cmc_curves_df = pd.DataFrame.from_dict(
        cmc_curves, orient="index", columns=[f"rank{i}" for i in range(1, 11)]
    )
    cmc_curves_df.index.name = "model"
    cmc_curves_df.reset_index().to_csv(
        "data/cmc_curves_comparison.tsv", sep="\t", index=False
    )
